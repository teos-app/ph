package core

import "fmt"

func NewPhApp() *PhApp {
	return &PhApp{}
}

type PhApp struct {
}

func (app *PhApp) Hello() {
	fmt.Println("Hello, World!")
}
