.PHONY: test quality build_macos build_windows build_linux

test:
	go test -v 2>&1 ./... | go-junit-report -set-exit-code > report.xml

quality:
	go vet ./...
	golint ./...
	gofmt -s -w .

build_macos:
	GOOS=darwin GOARCH=amd64 go build -o ./out/program-helper-macos-amd64 ./cmd

build_windows:
	GOOS=windows GOARCH=amd64 go build -o ./out/program-helper-windows-amd64.exe ./cmd

build_linux:
	GOOS=linux GOARCH=amd64 go build -o ./out/program-helper-linux-amd64 ./cmd
