# Requirements
## Debian/Ubuntu
~~~bash
sudo apt-get update
sudo apt-get install libgl1-mesa-dev
sudo apt-get install xorg-dev libx11-dev
~~~

## Fedora/Red Hat
~~~bash
sudo dnf install mesa-libGL-devel
sudo dnf install libX11-devel
~~~

## macOS
~~~bash
brew install glfw
~~~

## windows
Download and instal tdm-gcc
https://jmeubank.github.io/tdm-gcc/

~~~bash
go get -u github.com/go-gl/gl/v3.2-core/gl
go get -u github.com/go-gl/glfw/v3.3/glfw
~~~

